<?php
/**
* @autor Martin Candelario Vazquez Sanchez :)
*
*/

function getInventario($conection){
    $query = "SELECT i.id,
                    i.nombreProducto, 
                    i.referencia,
                    i.precio,
                    i.peso,
                    i.stock,
                    i.created_at,
                    i.ultimaVenta,
                    c.nombre
             FROM inventario i 
                INNER JOIN categoria c ON c.id = i.categoria_id";
    $inventarios = mysqli_query($conection,$query);
    
    return $inventarios;
}
