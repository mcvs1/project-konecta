<?php
/**
* @autor Martin Candelario Vazquez Sanchez :)
*
*/

include("database/db.php"); 
include("includes/header.php"); 



if(isset($_GET['id'])){
    $id = $_GET['id'];
    $query = "SELECT * FROM inventario WHERE id = $id";
    $queryCategoria = "SELECT * FROM categoria";
    $result = mysqli_query($conection, $query);
    $resultCategoria = mysqli_query($conection, $queryCategoria);

    if(mysqli_num_rows($result) == 1){
        $row = mysqli_fetch_array($result);
        $nombreProducto = $row['nombreProducto'];
        $referencia = $row['referencia'];
        $precio = $row['precio'];
        $peso = $row['peso'];
        $stock = $row['stock'];
        $created_at = date('Y-m-d H:m:s');
        $categoriaId = $row['categoria_id'];
        
    }   
}

if(isset($_POST['actualizar'])){
    
    $id = $_GET['id'];
    $nombreProducto = $_POST['nombreProducto'];
    $referencia = $_POST['referencia'];
    $precio = $_POST['precio'];
    $peso = $_POST['peso'];
    $stock = $_POST['stock'];
    $categoria =  $_POST['categoria'];
    $ultimaVenta = date('Y-m-d H:m:s');

    $updateInventario = "UPDATE inventario 
                            SET nombreProducto = '$nombreProducto', 
                                referencia = '$referencia', 
                                precio = '$precio',
                                peso = '$peso',
                                stock = '$stock',
                                ultimaVenta = '$ultimaVenta',
                                categoria_id = '$categoria' WHERE id = $id ";

    $resultado = mysqli_query($conection, $updateInventario);
    header("Location: index.php");
}

?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card-header">Editar inventario</div>
            <div class="card card-body">
                <form action="editar.php?id=<?= $_GET['id']; ?>" method="POST">
                    <div class="form-group">
                        <label for="nombreProducto">Nombre del producto</label>
                        <input type="text" name="nombreProducto" class="form-control" placeholder="" value="<?= $nombreProducto; ?>" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="referencia">Referencia</label>
                        <input type="text" name="referencia" class="form-control" placeholder="" value="<?= $referencia; ?>" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="text" name="precio" class="form-control" placeholder=""  value="<?= $precio; ?>" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="peso">Peso</label>
                        <input type="text" name="peso" class="form-control" placeholder="" value="<?= $peso; ?>" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="categoria">Categoría</label>
                        <select name="categoria" id="categoria" class="form-control">
                            <?php while($rows = mysqli_fetch_array($resultCategoria)){ ?>
                        
                            <option value="<?= $rows['id']; ?>" <?php if($row['categoria_id'] == $rows['id']){ echo 'selected'; } ?>><?= $rows['nombre']; ?></option>                            
                            <?php }?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="text" name="stock" class="form-control" placeholder="" value="<?= $stock; ?>" autofocus>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="actualizar" class="btn btn-info btn-block" value="actualizar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include("includes/footer.php"); ?>