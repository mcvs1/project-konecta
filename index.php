<?php
/**
* @autor Martin Candelario Vazquez Sanchez :)
*
*/

include("database/db.php"); 
include("includes/header.php"); 
include("./consultas/inventario.php"); 

$resultadoInventarios = getInventario($conection);

 ?>
<div class="row col-md-10 offset-md-1">
<div class="card card-header col-md-12">
        REGISTRAR EL INVENTARIO
    </div>
    <div class="card card-body">
                <form action="guardar.php" method="POST">
                    <div class="form-group">
                        <label for="nombreProducto">Nombre del producto</label>
                        <input type="text" name="nombreProducto" class="form-control" placeholder="" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="referencia">Referencia</label>
                        <input type="text" name="referencia" class="form-control" placeholder="" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="text" name="precio" class="form-control" placeholder="" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="peso">Peso</label>
                        <input type="text" name="peso" class="form-control" placeholder="" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="categoria">Categoría</label>
                        <select name="categoria" id="categoria" class="form-control">
                            <option value="1">categoria1</option>
                            <option value="2">categoria2</option>
                            <option value="3">categoria3</option>
                            <option value="4">categoria4</option>
                            <option value="5">categoria5</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="text" name="stock" class="form-control" placeholder="" autofocus>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="guardar" class="btn btn-info btn-block" value="Guardar">
                    </div>
                </form>
            </div>
</div>
    
            
        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td>Nombre del Producto</td>
                        <td>Referencia</td>
                        <td>Precio</td>
                        <td>Peso</td>
                        <td>Categoria</td>
                        <td>Stock</td>
                        <td>Fecha de Creación</td>
                        <td>Ultima Venta</td>
                        <td>Acciones</td>
                    </tr>                
                </thead>
                <tbody>
                <?php while($row = mysqli_fetch_array($resultadoInventarios)){  ?>
                    <tr>
                        <td><?=  $row['nombreProducto']; ?></td>
                        <td><?=  $row['referencia']; ?></td>
                        <td><?=  $row['precio']; ?></td>
                        <td><?=  $row['peso']; ?></td>
                        <td><?=  $row['nombre']; ?></td>
                        <td><?=  $row['stock']; ?></td>
                        <td><?=  $row['created_at']; ?></td>
                        <td><?=  $row['ultimaVenta']; ?></td>
                        <td> 
                            <a href="editar.php?id=<?= $row['id']; ?>" class="btn btn-success">Editar</a>
                            <a href="eliminar.php?id=<?= $row['id']; ?>" class="btn btn-danger">Eliminar</a>
                         </td>
                    </tr>

                <?php } ?>
                    <tr>
                    
                    </tr>
                </tbody>            
            </table>
        </div>
    </div>
</div>
<?php include("includes/footer.php"); ?>